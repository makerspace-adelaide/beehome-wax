<div align="center">

![](logo.svg)

</div>

---
A tool to convert [SPACE10 Beehomes](https://www.beehome.design) from DXF to SVG with context information.
Primary use is for [Easel](https://www.inventables.com/technologies/easel) import.

**Development halted until next use (if at all?)**

Primary mirror on [Codeberg](https://codeberg.org/MinervasRefuge/beehome-wax). Secondary on [GitLab](https://gitlab.com/makerspace-adelaide/beehome-wax).
