#!/usr/bin/python

#   Beehome Wax, Converts SPACE10 DXF to SVG with context information
#   Copyright (C) 2021  Abigale S. Raeck

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

#from __future__ import annotations
import ezdxf, argparse, typing, re, copy, sys, os  # type: ignore
from ezdxf.entities import DXFEntity  # type: ignore

import logging as l
from abc import ABC, abstractmethod
from io import TextIOBase
from math import isclose
from pprint import pprint
from typing import Union, Optional, Tuple, Any, List, Dict, Final, Iterator, Set, Iterable, Callable

#        =====================================
#        =           H e l p e r s           =
#        =====================================

Num = Union[int, float]
XY = Tuple[Num, Num]


class RebindNow():
  def __init__(self, nstream: TextIOBase, rbio):
    self.rbio = rbio
    self.prev = self.rbio.stream
    self.nstream = nstream

  def __enter__(self):
    self.rbio.stream = self.nstream

  def __exit__(self, type, value, traceback):
    self.rbio.stream = self.prev


class RebindableIO(TextIOBase):
  def __init__(self):
    self.stream = sys.stdout

  def rebind(self, s: TextIOBase):
    return RebindNow(s, self)

  def read(self, *args, **kwargs):
    return self.stream.read(*args, **kwargs)

  def readline(self, *args, **kwargs):
    return self.stream.readline(*args, **kwargs)

  def seek(self, *args, **kwargs):
    return self.stream.seek(*args, **kwargs)

  def tell(self, *args, **kwargs):
    return self.stream.tell(*args, **kwargs)

  def write(self, *args, **kwargs):
    return self.stream.write(*args, **kwargs)

  def __lshift__(self, s):
    self.write(s)
    return self


#rebindable IO
LIO: Final[RebindableIO] = RebindableIO()


def num(s: str) -> Num:
  """Attempts to parse number from string"""
  try:
    return int(s)
  except ValueError:
    return float(s)


def tagS(t: str, attr: Optional[Dict[str, str]] = None):
  """Writes XML start tag to LIO"""
  if attr is None:
    LIO << f"<{t}>"
  else:
    LIO << f"<{t} "

    for (k, v) in attr.items():
      LIO << f"{k}=\"{v}\" "

    LIO << ">"


def tagE(t: str):
  """Writes XML end tag to LIO"""
  LIO << f"</{t}>"


def tagIE(t: str, attr: Optional[Dict[str, str]] = None):
  """Writes XML single line tag to LIO"""
  if attr is None:
    LIO << f"<{t} />"
    return
  else:
    LIO << f"<{t} "

    for (k, v) in attr.items():
      LIO << f"{k}=\"{v}\" "

    LIO << "/>"


def tag(t: str, body: Callable[[], str], **kwargs):
  tagS(t, **kwargs)
  LIO << "\n"
  body()
  LIO << "\n"
  tagE(t)


def ison(a: XY, b: XY, **kwargs) -> bool:
  return isclose(a[0], b[0], **kwargs) and isclose(a[1], b[1], **kwargs)


#        =====================================
#        =           C l a s s e s           =
#        =====================================


class Size():
  def __init__(self, num: Num, units: str):
    self.num = num
    self.units = units

  def __repr__(self):
    return f"{self.num}{self.units}"


class CutType:
  def __init__(self, cut: str, side: str, toolsize: Size, depth: Size):
    self.cut = cut
    self.side = side
    self.toolsize = toolsize
    self.depth = depth

  def __repr__(self):
    return f"{self.cut} {self.side} -> {self.toolsize}, {self.depth}"

  def svg_attributes(self) -> dict[str, str]:
    """produces dict of attributes for Easel"""
    d = {}
    if self.side == "INSIDE":
      p = round(abs(self.depth.num - 30.0) / 30.0 * 0xFF)

      d["fill"] = f"rgb({p},{p},{p})"
      d["stroke"] = "none"
      d["class"] = "fill"
    elif self.side == "OUTSIDE":
      d["fill"] = "none"
      d["stroke"] = "#000"
      d["class"] = "outline"
    #d["stroke"]
    return d

  @staticmethod
  def parse(v: str) -> Optional[Any]:
    """Attempts to parse beehome layer name into respective cut type"""
    r = re.match(r"(\w+)-(\w+)_[t|T](\d+)(\w+?)?_(\d+(?:\.\d+)?)(\w+)", v)

    if r is not None:
      return CutType(r[1], r[2], Size(num(r[3]), "mm" if r[4] is None else r[4]), Size(num(r[5]), r[6]))
    else:
      return None


class BoundingBox():
  """ Creates a bounding box from the top left corner to the bottom right corner"""
  def __init__(self):
    self.is_init: bool = False
    self.ax: Num = 0
    self.bx: Num = 0
    self.ay: Num = 0
    self.by: Num = 0

  def expand(self, x: Num, y: Num):
    """expands bounding box to include the x,y point"""
    if self.is_init:
      self.ax = x if x < self.ax else self.ax
      self.bx = x if x > self.bx else self.bx

      self.ay = y if y > self.ay else self.ay
      self.by = y if y < self.by else self.by
    else:
      self.is_init = True
      self.ax = x
      self.bx = x
      self.ay = y
      self.by = y

  def exists_in(self, x: Num, y: Num) -> bool:
    """checks if x,y point exists within bounding box"""
    return self.is_init and\
           x >= self.ax and\
           x <= self.bx and\
           y <= self.ay and\
           y >= self.by

  def size2D(self) -> XY:
    """returns the x,y size of the bounding box"""
    return (abs(self.ax - self.bx), abs(self.ay - self.by))

  def centre(self) -> XY:
    """returns the centre point of the bounding box"""
    return ((self.ax + self.bx) / 2, (self.ay + self.by) / 2)

  def __repr__(self) -> str:
    if self.is_init:
      return f"{{({self.ax}, {self.ay}) -> ({self.bx}, {self.by}) size:{self.size2D()}}}"
    else:
      return "{BB not init}"

  def __hash__(self) -> int:
    return hash((self.ax, self.ay, self.bx, self.by))

  def __add__(self, b):
    a = copy.copy(self)
    a.expand(b.ax, b.ay)
    a.expand(b.bx, b.by)

    return a


class PathType(ABC):
  """abstract base class of SVG Path types"""
  def __init__(self, start: XY, end: XY):
    self.start: XY = start
    self.end: XY = end

  @abstractmethod
  def write_path(self) -> str:
    """returns a SVG path d attribute str of it's line type"""
    pass

  def __hash__(self) -> int:
    return hash((self.start, self.end))

  @abstractmethod
  def __repr__(self) -> str:
    pass


class PathLine(PathType):
  def write_path(self) -> str:
    return f"L {self.end[0]} {self.end[1]}"

  def __repr__(self) -> str:
    return f"({self.start} ― {self.end})"


class PathArc(PathType):
  def __init__(self, radius: Num, angle: Num, start: XY, end: XY):
    self.radius: Num = radius
    self.angle: Num = angle
    PathType.__init__(self, start, end)

  def write_path(self) -> str:
    fla = 0
    fs = 0 if self.angle > 0 else 1

    return f"A {self.radius} {self.radius} 0 {fla} {fs} {self.end[0]} {self.end[1]}"

  def __hash__(self) -> int:
    return PathType.__hash__(self) ^ hash((self.radius, self.angle))

  def __repr__(self) -> str:
    return f"({self.start} ⌒ {self.end}, {self.radius}, {abs(self.angle)})"


#         /\          /\          /\          /\          /\          /\          /\
#      /\//\\/\    /\//\\/\    /\//\\/\    /\//\\/\    /\//\\/\    /\//\\/\    /\//\\/\
#   /\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\\\///\\/\
#  //\\\//\/\\///\\\//\/\\///\\\//\/\\///\\\//\/\\///\\\//\/\\///\\\//\/\\///\\\//\/\\///\\
#  \\//\/                                                                            \/\\//
#   \/    ____  _____ _____ _   _  ___  __  __ _____          __        ___    __  __   \/
#   /\   | __ )| ____| ____| | | |/ _ \|  \/  | ____|         \ \      / / \   \ \/ /   /\
#  //\\  |  _ \|  _| |  _| | |_| | | | | |\/| |  _|    _____   \ \ /\ / / _ \   \  /   //\\
#  \\//  | |_) | |___| |___|  _  | |_| | |  | | |___  |_____|   \ V  V / ___ \  /  \   \\//
#   \/   |____/|_____|_____|_| |_|\___/|_|  |_|_____|            \_/\_/_/   \_\/_/\_\   \/
#   /\                                                                                  /\
#  //\\/\                                                                            /\//\\
#  \\///\\/\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\\\//
#   \/\\///\\\//\/\\///\\\//\/\\///\\\//\/\\///\\\//\/\\///\\\//\/\\///\\\//\/\\///\\\//\/
#      \/\\//\/    \/\\//\/    \/\\//\/    \/\\//\/    \/\\//\/    \/\\//\/    \/\\//\/
#         \/          \/          \/          \/          \/          \/          \/


def get_clean_paths(entity: DXFEntity, offset: XY) -> Iterator[PathType]:
  """Creates a generator that produces "clean" versions on LINE and ARC dxf entities"""
  for ve in entity.virtual_entities():
    if ve.dxf.dxftype == "LINE":
      ln = ve.dxf

      yield PathLine((ln.start.x - offset[0], ln.start.y - offset[1]), (ln.end.x - offset[0], ln.end.y - offset[1]))

    elif ve.dxf.dxftype == "ARC":
      a = ve.dxf

      si = [i for i in ve.angles(2)]
      sa = si[0]
      ea = si[1]

      angle = -abs(sa - ea)

      (sp, ep) = (ve.start_point, ve.end_point)
      (sp, ep) = ((sp.x - offset[0], sp.y - offset[1]), (ep.x - offset[0], ep.y - offset[1]))

      yield PathArc(a.radius, angle, sp, ep)


def order_paths(random_paths: Iterable[PathType]) -> List[PathType]:
  """Sort lines into an organised order makeing sure a.start -> a.end, a.end == b.start is consistant between shapes"""

  paths = []
  loose_paths = set(random_paths)

  #find a line to start from this will give us our inital direction
  #lines follow the same direction while traversing around the POLYLINE ~~Going round the twist!~~
  inital_path = [i for i in filter(lambda x: isinstance(x, PathLine), loose_paths)]

  if len(inital_path) == 0:
    raise Exception("POLYLINE has no LINE for lazy path organization")

  curr = inital_path[0]

  loose_paths.remove(curr)
  paths.append(curr)

  while len(loose_paths) > 0:
    #find a path where curr.end == path.start unless it's an arc, then also check the path.end (if flipping is needed)
    change = False

    for p in loose_paths:
      if ison(curr.end, p.start, abs_tol=0.0000001):
        change = True
        loose_paths.remove(p)
        paths.append(p)
        curr = p
        break

      elif isinstance(p, PathArc) and ison(curr.end, p.end, abs_tol=0.0000001):
        #flip arc
        change = True
        loose_paths.remove(p)
        paths.append(p)
        p.start, p.end = p.end, p.start
        curr = p
        p.angle *= -1
        break

    if not change:
      raise Exception(f"Line fails to find matching end curr:{curr} in {loose_paths}")

  return paths


def write_path(directional_path: Iterable[PathType], closedLoop: bool = True) -> Iterator[str]:
  "Generator that creates SVG path d attribute strings from a in order path"
  start = True

  for i in directional_path:
    if start:
      yield f"M {i.start[0]} {i.start[1]}"
      start = False

    yield i.write_path()

  if closedLoop:
    yield f"Z"


def generate_SVG_paths(entities: List[DXFEntity], offset: XY) -> Iterator[Tuple[CutType, Dict[str, str]]]:
  """Generator that produces cut type and svg path tags"""
  for e in entities:
    if e.dxf.dxftype == "POLYLINE" and (cut := CutType.parse(e.dxf.layer)) is not None:
      attr = cut.svg_attributes()
      attr["d"] = f"{' '.join(write_path(order_paths(get_clean_paths(e, offset))))}"
      attr["cut_type"] = repr(cut)

      yield (cut, attr)


def beehome_wax():
  """Main function, handles cmd line args and writes to files"""
  global LIO

  parser = argparse.ArgumentParser()
  parser.add_argument("file")
  args = parser.parse_args()

  l.basicConfig(level=l.INFO, format='[%(asctime)s] [%(levelname)s]: %(message)s')

  doc = ezdxf.readfile(args.file)
  ms = doc.modelspace()
  l.info(f"Imported DXF with {len(ms)} entities")

  groups: List[Tuple[BoundingBox, List[ezdxf.entities.DXFEntity]]] = []

  #find outside cut lines, create bounding box to dictate layers
  for e in ms:
    if e.dxf.layer.startswith("CUT"):
      bb = BoundingBox()
      groups.append((bb, []))

      for v in e.vertices:
        bb.expand(v.dxf.location.x, v.dxf.location.y)

  #TODO: correct for 7th layer (involving feet and stake)

  l.info(f"Discovered {len(groups)} groups")

  groups.sort(key=lambda x: x[0].centre()[1])

  #sort dxf elements into groups
  for e in filter(lambda a: a.dxf.dxftype not in ("3DSOLID"), ms):
    inside = False

    for (bb, g) in groups:
      if e.dxf.dxftype == "LWPOLYLINE":
        vertices = e.vertices()
      elif e.dxf.dxftype == "POLYLINE":
        vertices = [(v.dxf.location.x, v.dxf.location.y) for v in e.vertices]
      else:
        l.error(f"Unknown vertices type: {e}")
        break

      for (vx, vy) in vertices:
        if bb.exists_in(vx, vy):
          inside = True
          break

      if inside:
        g.append(e)

        #Expand bounding box if there are paths outside of outline (which there can be)
        for (vx, vy) in vertices:
          bb.expand(vx, vy)

        break

    if not inside:
      if hasattr(e, 'virtual_entities'):
        l.warning(f"Failed to find group for shape {e} lines:{len(list(e.virtual_entities()))}")
      else:
        l.warning(f"Failed to find group for shape {e}")

  #render groups in svg files
  layerdir = f"{args.file}-workspace"
  if not os.path.isdir(layerdir):
    os.mkdir(layerdir)

  for (i, (bb, es)) in enumerate(groups):
    l.info(f"Saving layer {i} lines:{len(es)}")

    with open(f"{layerdir}/layer-{i}.svg", "w") as f:
      with LIO.rebind(f):

        def svgTag():
          def gTag():
            op: List[Tuple[CutType, Dict[str, str]]] = [i for i in generate_SVG_paths(es, (bb.ax, bb.by))]
            op.sort(key=lambda x: x[0].depth.num)  #TODO: fix order rule, changes if layers move onto a different plank

            for (_, attr) in op:
              tagIE("path", attr=attr)

          tag("g", gTag, attr={"transform": f"scale(1 -1) translate(0 -{bb.size2D()[1]})"})  #Fixs x axis mirror

        tag("svg",
            svgTag,
            attr={
                "width": f"{bb.size2D()[0]}mm",
                "height": f"{bb.size2D()[1]}mm",
                "viewBox": f"0 0 {bb.size2D()[0]} {bb.size2D()[1]}"
            })

  #engrave cut vbit are splines, polylines, "LWPOLYLINE"

  #alt: produced JS to import into EASEL directly


if __name__ == "__main__":
  beehome_wax()
